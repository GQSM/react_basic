const menuData = [
    {id:'sys',parent:'',name:'系統設定',url:'#'},
    {id:'sys01',parent:'sys',name:'系統設定1',url:'sys1'},
    {id:'sys02',parent:'sys',name:'系統設定2',url:'sys1'},
    {id:'fic',parent:'',name:'頁面資訊管理',url:'#'},
    {id:'fic01',parent:'fic',name:'頁面資訊管理01',url:'fic1'},
    {id:'fic02',parent:'fic',name:'頁面資訊管理02',url:'fic2'},
]

export {menuData}