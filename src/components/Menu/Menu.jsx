import React from 'react'
import { connect } from 'react-redux'
import styles from './scss/index.scss'

const MenuItem = (props) => {
    return (
        <div class='panel panel-default'>
            <Header menu={props.parentData} />
            <Items parentId={props.parentData.id} itemsData={props.menuData} />
        </div>
    )
}

const Header = (props) => {
    return (
        <div class='panel-heading'>
            <h4 class='panel-title'>
                <a data-toggle='collapse' data-parent='#accordion' href={`#${props.menu.id}`}>
                    {props.menu.name}
                </a>
            </h4>
        </div>
    )
}

const Items = (props) => {
    let items = props.itemsData.map((item) => {
        if(item.parent === props.parentId){
            return <Item itemData = {item} />
        }
    })
    return (
        <div id={props.parentId} class='panel-collapse collapse'>
            <div className={styles['panel-body']}>
                <table className={`${styles.table}  table`}>
                    <tbody>
                        {items}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

const Item = (props) => {
    return (
        <tr>
            <td>
                <a href={props.itemData.url}>{props.itemData.name}</a>
            </td>
        </tr>
    )
}

class ConnectMenu extends React.Component {
    render() {
        const MenuItems = this.props.data.map((itemData) => {
            if (itemData.parent === '')
                return <MenuItem parentData={itemData} menuData={this.props.data} />
        })

        return (
            <div class='panel-group' id='accordion'>
                {MenuItems}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return { data: state }
}

const Menu = connect(mapStateToProps)(ConnectMenu)


export { Menu }