import { menuReducer } from "../reducers"
import { createStore } from "redux"

const menuStore = createStore(menuReducer)

window.store = menuStore

export {menuStore}
