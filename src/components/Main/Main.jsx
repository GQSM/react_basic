import React from "react"
import { Provider } from "react-redux"
import { menuStore } from "../../stores"
import { Menu } from "../Menu"
import { Content } from "../Content"

class Main extends React.Component {
    render() {
        return (
            <Provider store={menuStore}>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3 col-md-3">
                            <Menu />
                        </div>
                        <div class="col-sm-9 col-md-9">
                            <Content />
                        </div>
                    </div>
                </div>
            </Provider>
        )
    }
}

export { Main }