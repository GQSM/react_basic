import React from "react"
import styles from './scss/index.scss'

class Login extends React.Component {

    constructor(props) {
        super(props)
        this.state = { userAccount: '', userPassword: '' }
        this.handleChange = this.handleChange.bind(this)
        this.checkLogin = this.checkLogin.bind(this)
        this.login = this.login.bind(this)
    }

    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    checkLogin() {
        fetch('http://smallfishit.com/Back/Login.php', {
            method: 'POST',
            headers: new Headers({
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin':'*'
            }),
            body: JSON.stringify({
                account: this.state.userAccount,
                password: this.state.userPassword,
                usertype: '1'
            })
        }).then(function (response) {
            response.json()
        }).then(function (myJson) {
            console.log(myJson)
        }).catch(function (err) {
            console.log(err)
        })
    }

    login() {
        if (!grecaptcha.getResponse()) {
            alert('請勾選「我不是機器人」')
            return false;
        }
        this.checkLogin()
    }

    render() {
        return (
            <div class="container-fluid">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4">
                            <div className={styles.caixa}>
                                <h1 class="text-center txt-caixa">
                                    書城後台管理系統
                            </h1>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <fieldset className={styles.formRow}>
                                            <div className={styles['formRow--item']}>
                                                <label for="firstname" className={`${styles['formRow--input-wrapper']} js-inputWrapper`}>
                                                    <input type="email" className={`${styles['formRow--input']} js-input`} name="userAccount" placeholder="使用者帳號" onChange={this.handleChange} />
                                                </label>
                                            </div>
                                        </fieldset>
                                        <fieldset className={styles.formRow}>
                                            <div className={styles['formRow--item']}>
                                                <label for="firstname" className={`${styles['formRow--input-wrapper']} js-inputWrapper`}>
                                                    <input type="password" className={`${styles['formRow--input']} js-input`} name="userPassword" placeholder="使用者密碼" onChange={this.handleChange} />
                                                </label>
                                            </div>
                                        </fieldset>
                                        <div className={styles.recaptcha}>
                                            <div class="g-recaptcha" data-sitekey="6Le6pn8UAAAAAGE6Pz4omNoJBUQTRMjxO1mAHtl7"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <button type="button" className={styles.vamos_mudar_um_pouco} title="Entrar" onClick={this.login}>登入</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4"></div>
                    </div>
                </div>
            </div>
        )
    }
}

export { Login }