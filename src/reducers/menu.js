import { menuData } from "../contents/menu.js"

const menuReducer = (state = menuData, action) => {
    switch (action.type) {
        default: {
            return state
        }
    }
}

window.menuData = menuData

export { menuReducer }